var gulp = require('gulp')
var sass = require('gulp-sass')

gulp.task('compile-sass', function() {
  return gulp.src('wagtailemoji/static_src/wagtailemoji/scss/**/*.scss')
    .pipe(sass({
      includePaths: ['node_modules'],
    }))
    .pipe(gulp.dest('wagtailemoji/static/wagtailemoji/css'))
})

gulp.task('copy-fonts', function() {
  return gulp.src('wagtailemoji/static_src/wagtailemoji/fonts/**/*')
    .pipe(gulp.dest('wagtailemoji/static/wagtailemoji/fonts'))
})

gulp.task('default', ['compile-sass', 'copy-fonts'])
